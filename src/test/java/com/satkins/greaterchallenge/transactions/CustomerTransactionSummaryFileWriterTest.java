package com.satkins.greaterchallenge.transactions;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerTransactionSummaryFileWriterTest {

    @Autowired
    private TestCustomerTransactionSummaryFileWriter fileWriter;
    
    @Test
    public void ifNoTransactions_summaryEmpty() throws Exception {
        final TransactionFileSummary summary = new TransactionFileSummary("testFilename.csv");
        fileWriter.writeSummary(summary);
        
        final String summaryOutput = fileWriter.getStringWriter().toString();
        assertThat(summaryOutput)
            .contains("File Processed: testFilename.csv")
            .contains("Total Accounts: 0")
            .contains("Total Credits : $0.00")
            .contains("Total Debits : $0.00")
            .contains("Skipped Transactions: 0");
    }
    
    @Test
    public void ifInvalidTransaction_correctCounts() throws Exception {
        final TransactionFileSummary summary = new TransactionFileSummary("testFilename.csv");
        final Transaction transaction = new Transaction(false);
        summary.addTransactionToSummary(transaction);
        
        fileWriter.writeSummary(summary);
        
        final String summaryOutput = fileWriter.getStringWriter().toString();
        assertThat(summaryOutput)
            .contains("File Processed: testFilename.csv")
            .contains("Total Accounts: 0")
            .contains("Total Credits : $0.00")
            .contains("Total Debits : $0.00")
            .contains("Skipped Transactions: 1");
    }
    
    @Test
    public void ifDebitTransaction_correctCounts() throws Exception {
        final TransactionFileSummary summary = new TransactionFileSummary("testFilename.csv");
        final Transaction transaction = new Transaction(123456L, BigDecimal.ONE.negate());
        
        summary.addTransactionToSummary(transaction);
        
        fileWriter.writeSummary(summary);
        
        final String summaryOutput = fileWriter.getStringWriter().toString();
        assertThat(summaryOutput)
            .contains("File Processed: testFilename.csv")
            .contains("Total Accounts: 1")
            .contains("Total Credits : $0.00")
            .contains("Total Debits : $1.00")
            .contains("Skipped Transactions: 0");
    }
    
    @Test
    public void ifCreditTransaction_correctCounts() throws Exception {
        final TransactionFileSummary summary = new TransactionFileSummary("testFilename.csv");
        final Transaction transaction = new Transaction(123456L, BigDecimal.ONE);
        summary.addTransactionToSummary(transaction);
        
        fileWriter.writeSummary(summary);
        
        final String summaryOutput = fileWriter.getStringWriter().toString();
        assertThat(summaryOutput)
            .contains("File Processed: testFilename.csv")
            .contains("Total Accounts: 1")
            .contains("Total Credits : $1.00")
            .contains("Total Debits : $0.00")
            .contains("Skipped Transactions: 0");
    }
    
    @Test
    public void ifCreditTransactionForOverOneMillion_moneyFormatCorrect() throws Exception {
        final TransactionFileSummary summary = new TransactionFileSummary("testFilename.csv");
        final Transaction transaction = new Transaction(123456L, new BigDecimal("1234567.59"));
        summary.addTransactionToSummary(transaction);
        
        fileWriter.writeSummary(summary);
        
        final String summaryOutput = fileWriter.getStringWriter().toString();
        assertThat(summaryOutput)
            .contains("Total Credits : $1,234,567.59");
    }
    
    @Test
    public void ifMultipleRecords_allIncludedInSummary() throws Exception {
        final TransactionFileSummary summary = new TransactionFileSummary("testFilename.csv");
        final Transaction transaction = new Transaction(123456L, BigDecimal.ONE.negate());
        final Transaction transaction2 = new Transaction(654321L, BigDecimal.ONE);

        summary.addTransactionToSummary(transaction);
        summary.addTransactionToSummary(transaction2);
        
        fileWriter.writeSummary(summary);
        
        final String summaryOutput = fileWriter.getStringWriter().toString();
        assertThat(summaryOutput)
            .contains("File Processed: testFilename.csv")
            .contains("Total Accounts: 2")
            .contains("Total Credits : $1.00")
            .contains("Total Debits : $1.00")
            .contains("Skipped Transactions: 0");
    }
}
