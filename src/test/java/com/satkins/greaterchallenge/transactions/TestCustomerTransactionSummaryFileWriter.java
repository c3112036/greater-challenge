package com.satkins.greaterchallenge.transactions;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service
public class TestCustomerTransactionSummaryFileWriter extends CustomerTransactionSummaryFileWriter {
    private StringWriter stringWriter;
    
    @Override
    Writer getSummaryFileWriter(final String summaryFilename) throws IOException {
        stringWriter = new StringWriter();
        return stringWriter;
    }
    
    public StringWriter getStringWriter() {
        return stringWriter;
    }
}
