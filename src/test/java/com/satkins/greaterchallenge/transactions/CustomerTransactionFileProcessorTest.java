package com.satkins.greaterchallenge.transactions;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.Reader;
import java.io.StringReader;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerTransactionFileProcessorTest {
    @Autowired private CustomerTransactionFileProcessor processor;
    
    @Test
    public void ifEmptyTransactionFile_defaultValuesInSummary() throws Exception {
        final Reader stringReader = new StringReader("");
        
        final TransactionFileSummary summary = 
            processor.processFile(stringReader, "testFilename.csv");
        
        assertThat(summary).isNotNull();
        assertThat(summary.getNumberOfAccounts()).isEqualTo(0);
    }
    /*
    @Test
    public void ifHeaderOnlyTransactionFile_defaultValuesInSummary() throws Exception {
        final Reader stringReader = new StringReader("\"Customer Account#\", \"Transaction Amount\"");
        
        final TransactionFileSummary summary = 
            processor.processFile(stringReader, "testFilename.csv");
        
        assertThat(summary).isNotNull();
        assertThat(summary.getNumberOfAccounts()).isEqualTo(0);
    }
    
    @Test
    public void ifNonNumericAccountNumber_skippedCountInSummary() throws Exception {
        final Reader stringReader = 
                new StringReader(
                    "\"Customer Account#\", \"Transaction Amount\"\r\n" +
                    "\"123A\",\"150.00\"");
        final TransactionFileSummary summary = 
            processor.processFile(stringReader, "testFilename.csv");
        
        assertThat(summary).isNotNull();
        assertThat(summary.getNumberOfAccounts()).isEqualTo(0);
        assertThat(summary.getNumberOfSkippedRecords()).isEqualTo(1);
    }
    
    @Test
    public void ifMixOfRecordTypes_successfullyProcessed() throws Exception {
        final Reader stringReader = 
                new StringReader(
                    "\"Customer Account#\", \"Transaction Amount\"\r\n" +
                    "\"123A\",\"150.00\"\r\n" +
                    "\"1231\",\"10\"\r\n" +
                    "\"1232\",\"100.00\"\r\n" +
                    "\"1231\",\"160.65\"\r\n" +
                    "\"1231\",\"-200.00\"\r\n" +
                    "\"1232\",\"-103.00\"\r\n" +
                    "\"1233\",\"+150.00\"\r\n");
        final TransactionFileSummary summary = 
            processor.processFile(stringReader, "testFilename.csv");
        
        assertThat(summary).isNotNull();
        assertThat(summary.getNumberOfAccounts()).isEqualTo(3);
        assertThat(summary.getNumberOfSkippedRecords()).isEqualTo(1);
        assertThat(summary.getTotalCreditAmount()).isEqualByComparingTo(new BigDecimal("420.65"));
        assertThat(summary.getTotalDebitAmount()).isEqualByComparingTo(new BigDecimal("-303"));
    }*/
}
