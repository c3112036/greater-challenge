package com.satkins.greaterchallenge.transactions;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.Test;

public class TransactionFileSummaryTest {

    @Test
    public void ifTransactionFileSummaryIninitialised_correctDefaults() {
        final TransactionFileSummary summary = new TransactionFileSummary("testFilename.csv");
        
        assertThat(summary.getFilename()).isEqualTo("testFilename.csv");
        assertThat(summary.getNumberOfAccounts()).isEqualTo(0);
        assertThat(summary.getNumberOfSkippedRecords()).isEqualTo(0);
        assertThat(summary.getTotalCreditAmount()).isEqualTo(BigDecimal.ZERO);
        assertThat(summary.getTotalDebitAmount()).isEqualTo(BigDecimal.ZERO);
    }
    
    @Test
    public void ifInvalidTransaction_correctCounts() {
        final TransactionFileSummary summary = new TransactionFileSummary("testFilename.csv");
        final Transaction transaction = new Transaction(false);
        
        summary.addTransactionToSummary(transaction);
        
        assertThat(summary.getNumberOfAccounts()).isEqualTo(0);
        assertThat(summary.getNumberOfSkippedRecords()).isEqualTo(1);
        assertThat(summary.getTotalCreditAmount()).isEqualTo(BigDecimal.ZERO);
        assertThat(summary.getTotalDebitAmount()).isEqualTo(BigDecimal.ZERO);
    }
    
    @Test
    public void ifDebitTransaction_correctCounts() {
        final TransactionFileSummary summary = new TransactionFileSummary("testFilename.csv");
        final Transaction transaction = new Transaction(123456L, BigDecimal.ONE.negate());
        
        summary.addTransactionToSummary(transaction);
        
        assertThat(summary.getFilename()).isEqualTo("testFilename.csv");
        assertThat(summary.getNumberOfAccounts()).isEqualTo(1);
        assertThat(summary.getNumberOfSkippedRecords()).isEqualTo(0);
        assertThat(summary.getTotalCreditAmount()).isEqualTo(BigDecimal.ZERO);
        assertThat(summary.getTotalDebitAmount()).isEqualTo(BigDecimal.ONE.negate());
    }
    
    @Test
    public void ifCreditTransaction_correctCounts() {
        final TransactionFileSummary summary = new TransactionFileSummary("testFilename.csv");
        final Transaction transaction = new Transaction(123456L, BigDecimal.ONE);
        
        summary.addTransactionToSummary(transaction);
        
        assertThat(summary.getFilename()).isEqualTo("testFilename.csv");
        assertThat(summary.getNumberOfAccounts()).isEqualTo(1);
        assertThat(summary.getNumberOfSkippedRecords()).isEqualTo(0);
        assertThat(summary.getTotalCreditAmount()).isEqualTo(BigDecimal.ONE);
        assertThat(summary.getTotalDebitAmount()).isEqualTo(BigDecimal.ZERO);
    }
    
    @Test
    public void ifConsecutiveRecordsForSameAccount_countedOnce() {
        final TransactionFileSummary summary = new TransactionFileSummary("testFilename.csv");
        final Transaction transaction = new Transaction(123456L, BigDecimal.ONE);
        
        summary.addTransactionToSummary(transaction);
        summary.addTransactionToSummary(transaction);
        
        assertThat(summary.getFilename()).isEqualTo("testFilename.csv");
        assertThat(summary.getNumberOfAccounts()).isEqualTo(1);
        assertThat(summary.getNumberOfSkippedRecords()).isEqualTo(0);
        assertThat(summary.getTotalCreditAmount()).isEqualTo(new BigDecimal( 2 ));
        assertThat(summary.getTotalDebitAmount()).isEqualTo(BigDecimal.ZERO);
    }
    
    @Test
    public void ifSeparatedRecordsForSameAccount_countedOnce() {
        final TransactionFileSummary summary = new TransactionFileSummary("testFilename.csv");
        final Transaction transaction = new Transaction(123456L, BigDecimal.ONE);
        final Transaction otherTransaction = new Transaction(654321L, BigDecimal.TEN.negate());
        
        summary.addTransactionToSummary(transaction);
        summary.addTransactionToSummary(otherTransaction);
        summary.addTransactionToSummary(transaction);
        
        assertThat(summary.getFilename()).isEqualTo("testFilename.csv");
        assertThat(summary.getNumberOfAccounts()).isEqualTo(2);
        assertThat(summary.getNumberOfSkippedRecords()).isEqualTo(0);
        assertThat(summary.getTotalCreditAmount()).isEqualTo(new BigDecimal( 2 ));
        assertThat(summary.getTotalDebitAmount()).isEqualTo(BigDecimal.TEN.negate());
    }
}
