package com.satkins.greaterchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

import com.satkins.greaterchallenge.filelistener.CustomerTransactionFileListener;

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class GreaterChallengeApplication {
    
    public static void main(final String[] args) throws Exception{
        final ConfigurableApplicationContext context = 
            SpringApplication.run(GreaterChallengeApplication.class, args);
        
        final CustomerTransactionFileListener fileListener = 
            context.getBean(CustomerTransactionFileListener.class);
        fileListener.begin();
    }
}
