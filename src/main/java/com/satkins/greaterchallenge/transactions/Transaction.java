package com.satkins.greaterchallenge.transactions;

import java.math.BigDecimal;

import org.apache.commons.csv.CSVRecord;
import org.springframework.util.StringUtils;

public class Transaction {
    private final boolean validTransaction;
    private Long customerAccountNumber;
    private BigDecimal amount;
    
    Transaction(final boolean invalid)
    {
        validTransaction = false;
        customerAccountNumber = null;
        amount = null;
    }
    
    Transaction(final Long customerAccountNumber,
                final BigDecimal amount) {
        validTransaction = true;
        this.customerAccountNumber = customerAccountNumber;
        this.amount = amount;
    }
    
    public boolean isValidTransaction() {
        return validTransaction;
    }
    
    public Long getCustomerAccountNumber() {
        return customerAccountNumber;
    }
    
    public void setCustomerAccountNumber(final Long customerAccountNumber) {
        this.customerAccountNumber = customerAccountNumber;
    }
    
    public BigDecimal getAmount() {
        return amount;
    }
    
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public static Transaction fromCsvRecord(final CSVRecord record) {
        try {
            return new Transaction(
                Long.parseLong(StringUtils.trimWhitespace(record.get(0))),
                new BigDecimal(StringUtils.trimWhitespace(record.get(1))));
        }
        catch (final Exception e) {
            return new Transaction(false);
        }
    }
}
