package com.satkins.greaterchallenge.transactions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CustomerTransactionProcessedFileMover {
    @Value("${$TRANSACTION_PROCESSING}")
    private String transactionProcessingDirectory;

    public void moveToProcessedDirectory(final Path incomingFile) throws IOException {
        Files.move(
            incomingFile,
            Paths.get(
                transactionProcessingDirectory + 
                    "/processed/" + incomingFile.getFileName()));
    }
}
