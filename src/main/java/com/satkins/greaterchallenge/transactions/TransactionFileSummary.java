package com.satkins.greaterchallenge.transactions;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class TransactionFileSummary {
    private final Set<Long> observedAccounts;
    private int skippedRecordCount;
    private BigDecimal totalCreditAmount;
    private BigDecimal totalDebitAmount;
    
    private final String filename;
    
    public TransactionFileSummary(final String filename) {
        observedAccounts = new HashSet<>();
        skippedRecordCount = 0;
        totalCreditAmount = BigDecimal.ZERO;
        totalDebitAmount = BigDecimal.ZERO;
        
        this.filename = filename;
    }
    
    public void addTransactionToSummary(final Transaction transaction) {
        if (transaction.isValidTransaction()) {
            observedAccounts.add(transaction.getCustomerAccountNumber());
            
            if (transaction.getAmount().compareTo(BigDecimal.ZERO) > 0) {
                totalCreditAmount = totalCreditAmount.add(transaction.getAmount());
            }
            else {
                totalDebitAmount = totalDebitAmount.add(transaction.getAmount());
            }
        }
        else {
            skippedRecordCount++;
        }
    }
    public int getNumberOfAccounts() {
        return observedAccounts.size();
    }
    
    public int getNumberOfSkippedRecords() {
        return skippedRecordCount;
    }
    
    public BigDecimal getTotalCreditAmount() {
        return totalCreditAmount;
    }

    public BigDecimal getTotalDebitAmount() {
        return totalDebitAmount;
    }

    public String getFilename() {
        return filename;
    }
}
