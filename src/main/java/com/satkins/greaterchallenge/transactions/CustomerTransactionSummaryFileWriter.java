package com.satkins.greaterchallenge.transactions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CustomerTransactionSummaryFileWriter {
    
    private static final String NEWLINE = "\r\n";
    
    @Value("${$TRANSACTION_PROCESSING}")
    private String transactionProcessingDirectory;
    
    @Value("${customerTransactionFileRegex}")
    private String customerTransactionFileRegex;
    
    public void writeSummary(final TransactionFileSummary summary) throws IOException
    {
        final NumberFormat integerFormat = new DecimalFormat("###,###");
        final NumberFormat amountFormat = new DecimalFormat("$###,##0.00");
        
        try (Writer writer = getSummaryFileWriter(summary.getFilename())) {
            writer.write( String.format("File Processed: %s%s", summary.getFilename(), NEWLINE));
            writer.write(String.format("Total Accounts: %s%s", integerFormat.format(summary.getNumberOfAccounts()), NEWLINE));
            writer.write(String.format("Total Credits : %s%s", amountFormat.format(summary.getTotalCreditAmount()), NEWLINE));
            writer.write(String.format("Total Debits : %s%s", amountFormat.format(summary.getTotalDebitAmount().negate()), NEWLINE));
            writer.write(String.format("Skipped Transactions: %s", integerFormat.format(summary.getNumberOfSkippedRecords())));
            
            writer.flush();
        }
    }
    
    private String determineSummaryFilename(final String transactionFilename) {
        final Matcher filenameMatcher = 
            Pattern.compile(customerTransactionFileRegex)
                .matcher(transactionFilename);
        
        // This should always be the case because we wouldn't receive the file if the
        // name was not as expected
        filenameMatcher.matches();
        return String.format("finance_customer_transactions_report-%s.txt", filenameMatcher.group(1));
    }
    
    // This method exists to facilitate easier testing of our summary writer (without using mock libraries)
    Writer getSummaryFileWriter(final String transactionFilename) throws IOException {
        final File file = 
            new File(
                transactionProcessingDirectory + "/reports/" + determineSummaryFilename(transactionFilename));
        if (!file.createNewFile()) {
            throw new IllegalStateException("Existing summary file exists");
        }
        
        return new FileWriter(file);
    }
}
