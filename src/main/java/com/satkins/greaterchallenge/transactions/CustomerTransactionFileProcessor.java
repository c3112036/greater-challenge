package com.satkins.greaterchallenge.transactions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

@Service
public class CustomerTransactionFileProcessor {

    public TransactionFileSummary processFile(
            final Reader incomingFileReader,
            final String filename) throws IOException
    {
        final TransactionFileSummary summary = 
                new TransactionFileSummary(filename);

        try (final BufferedReader bufferedReader = 
                new BufferedReader(incomingFileReader)) {

            for (final CSVRecord record : CSVFormat.DEFAULT.parse(bufferedReader)) {
                if (record.getRecordNumber() == 1) {
                    // For now we just skip our header line
                    continue; 
                }

                final Transaction transaction = Transaction.fromCsvRecord(record);
                summary.addTransactionToSummary(transaction);
            }
        }

        return summary;
    }

    public TransactionFileSummary processFile(final Path newFilePath) throws FileNotFoundException, IOException {
        try (final Reader reader = new FileReader(newFilePath.toFile())) {
            return processFile(reader, newFilePath.getFileName().toString());
        }
    }
}
