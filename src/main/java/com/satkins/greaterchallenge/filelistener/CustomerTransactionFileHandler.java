package com.satkins.greaterchallenge.filelistener;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.satkins.greaterchallenge.transactions.CustomerTransactionFileProcessor;
import com.satkins.greaterchallenge.transactions.CustomerTransactionProcessedFileMover;
import com.satkins.greaterchallenge.transactions.CustomerTransactionSummaryFileWriter;
import com.satkins.greaterchallenge.transactions.TransactionFileSummary;

@Service
public class CustomerTransactionFileHandler {
    @Value("${customerTransactionFileRegex}")
    private String customerTransactionFileRegex;
    
    @Autowired private CustomerTransactionFileProcessor transactionFileProcessor;
    @Autowired private CustomerTransactionSummaryFileWriter transactionSummaryWriter;
    @Autowired private CustomerTransactionProcessedFileMover fileMover;
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    public void handleNewFile(final Path newFilePath) throws FileNotFoundException, IOException {
        if (!newFilePath.getFileName().toString().matches(customerTransactionFileRegex)) {
            logger.info(
                String.format("Encountered file '%s' which did not match expected pattern", newFilePath.toString()));
        }
        else {
            final TransactionFileSummary summary = 
                transactionFileProcessor.processFile(newFilePath);
            transactionSummaryWriter.writeSummary(summary);
            fileMover.moveToProcessedDirectory(newFilePath);
            
        }
    }
}
