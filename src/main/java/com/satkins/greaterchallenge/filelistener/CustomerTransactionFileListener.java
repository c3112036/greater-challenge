package com.satkins.greaterchallenge.filelistener;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

@Configuration
@EnableAsync
@Service
public class CustomerTransactionFileListener {
    @Value("${$TRANSACTION_PROCESSING}")
    private String transactionProcessingDirectory;
    
    @Autowired CustomerTransactionFileHandler fileHandler;
    
    @Async
    public void begin() throws Exception {
        final WatchService watcher = 
            FileSystems.getDefault().newWatchService();
        
        final Path processingDirectory =
            Paths.get(transactionProcessingDirectory + "/pending");
        
        processingDirectory.register(watcher, ENTRY_CREATE);
        
        while (true) {
            WatchKey key;
            try {
                key = watcher.take();
            } 
            catch (final InterruptedException ex) {
                return;
            }
         
            for (final WatchEvent<?> event : key.pollEvents()) {
                final WatchEvent.Kind<?> kind = event.kind();
         
                @SuppressWarnings("unchecked")
                final WatchEvent<Path> ev = (WatchEvent<Path>) event;
                
                final Path fileDirectory = (Path)key.watchable();
                final Path fullPath = fileDirectory.resolve(ev.context());
                
                // We can have instances where the file remains lock (on Windows) 
                // for a short period after the file is created. We loop
                int waitCounter = 0;
                
                while (true) {
                    final File file = fullPath.toFile();
                    
                    if (file.renameTo(file)) {
                        break;
                    }
                    
                    Thread.sleep(100);
                    
                    if (waitCounter++ > 50) {
                        throw new IllegalStateException("Cannot acquire file lock");
                    }
                }
                
                if (kind == ENTRY_CREATE) {
                    fileHandler.handleNewFile(fullPath);
                }
            }
         
            // Reset key so we are ready for new events
            final boolean valid = key.reset();
            if (!valid) {
                throw new IllegalStateException(
                    "Cannot continue watching for file events. " +
                    "Has the directory been deleted?");
            }
        }
    }
}
