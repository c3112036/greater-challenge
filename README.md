# Submission Information
## Running/Building:
To compile + test + build the application execute: `gradlew build`. This will produce a `<currentDir>\build\libs` folder containing the jar. In the interest of making it as simple as possible to run I've included the `\build\libs` folder along with the `pending`, `processed` and `reports` directories.

To run the application: `java -jar greater-challenge-0.0.1-SNAPSHOT.jar`. `$TRANSACTION_PROCESSING` will be assumed to be the same as the jar's directory. To select a directory other than the jar�s directory for the $TRANSACTION_PROCESSING argument: `java -jar greater-challenge-0.0.1-SNAPSHOT.jar -e $TRANSACTION_PROCESSING:<desiredDirectory>`

## Approach:
While the tempatation was there to complete this sample in one or two Java classes I opted instead to use Spring Boot, and Spring's dependency
injection functionality to start making logical separations between components. This also played nicely into my preferred test-based development.
The possibility to split the application differently, or split it further is definitely present. Without understanding how the Greater likes to 
approach this particular concern I went with a fairly simple approach.

## Limitations:
While implementing the prototype there were a number of items that did not get fully polished given the focus was largely on demonstrating how this small example would work in principal. The following limitations exist:

 1. The `pending`, `processed` and `reports` directories must exist as sub directories of $TRANSACTION_PROCESSING prior to running the application
 2. I opted to use a `WatchService` rather than polling the pending directory as it better handles the "Files must be processed with 5 minutes" requirement. With that said, files placed in that directory while the application is not running will not be processed. More on this below.
 3. While each file placed in the pending directory will only be processed once, the lack of a database to properly record files that have been processed means I opted for a rudimentary restriction that simply throws an exception if there was a file with the same name when writing the report file. Any solution though that relies on the file system for this requirement will not be scalable, hence my opting to avoid implementing something more robust in this instance.
 
## Elaborating on some of the points above:
- An alternative to the WatchService solution I opted for was to create a scheduler that woke up at selected intervals to poll the file system for changes. I didn't like this approach for two reasons:
    - We want to be processing as soon as we can, and;
    - If you wanted to run the example it would have been painful to wait for the scheduled polling times to roll around (unless I made them frequent)
    
    A nice "both worlds" solution here might be to retain the WatchService but introduce scheduled notifications that alert individuals about the lack of a transactions file being received.
	
- I mentioned above that using the file system to stop duplicate processing was not scalable. Assuming a requirement existed where files with duplicate contents (regardless of their names) should be ignored, we might opt to pre-parse files to calculate a hash. This hash would then form the basis of our database backed duplicate check.

- The necessity to only add files to the pending directory while the application is running not a Production worthy solution (but suffices for a prototype). We could alleviate these concerns by introducing a startup task that first checked the directory for outstanding files that had been received while the application was down.
 
Thanks for considering the sample I've submitted.

Regards,
Sean